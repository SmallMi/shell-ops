#!/usr/bin/env bash
for i in `cat hostlist.txt`
do
ip=`echo $i | cut -d \, -f 2`
hostname=`echo $i | cut -d \, -f 1`
ansible $ip -m script -a "/tmp/rename.sh $ip $hostname"
done