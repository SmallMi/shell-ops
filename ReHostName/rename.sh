#!/usr/bin/env bash

ip=$1
hostname=$2

hostname $hostname
echo $ip $hostname >> /etc/hosts
sed -i "s/HOSTNAME=localhost/HOSTNAME=$hostname/g" /etc/sysconfig/network