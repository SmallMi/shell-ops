#!/bin/bash
##  2018-03-26
## http://www.smallmi.com
##email: 1374409400@qq.com
##Seven Chou
##centos 6

zkServer=/oriental/zookeeper/bin/zkServer.sh

case "$1" in
 install)
    resp_code=`curl -I -m 10 -o /dev/null -s -w %{http_code} "http://192.168.201.53:8888/zookeeper-3.4.10.tar.gz"`

    echo '开始下载zookeeper，请稍等...'
    sleep 3
    if [ $resp_code == 200 ];then
        wget http://192.168.201.53:8888/zookeeper-3.4.10.tar.gz
    else
        wget http://archive.apache.org/dist/zookeeper/zookeeper-3.4.10/zookeeper-3.4.10.tar.gz
    fi

    echo '开始解压zookeeper到/oriental/并重命名，请稍等...'
    sleep 3
    tar fxz zookeeper-3.4.10.tar.gz -C /oriental/
    mv /oriental/zookeeper-3.4.10 /oriental/zookeeper

    #touch conf/zoo.cfg
    echo '开始生成zookeeper配置文件到/oriental/zookeeper/conf/zoo.cfg，请稍等...'
    sleep 3
    echo -e "tickTime=2000\ndataDir=/ori-data/zookeeper/data\ndataLogDir=/ori-data/zookeeper/logs\nclientPort=2181">>/oriental/zookeeper/conf/zoo.cfg

    echo '开始创建zookeeper数据目录/ori-data/zookeeper/data及日志目录/ori-data/zookeeper/logs，请稍等...'
    sleep 3
    mkdir -p /ori-data/zookeeper/data
    mkdir -p /ori-data/zookeeper/logs

    echo '设置zookeeper输入日志级别及路径...'
    sleep 3
    sed -i 's/ZOO_LOG_DIR=\".\"/ZOO_LOG_DIR=\"\/ori-data\/zookeeper\/logs\"/g' /oriental/zookeeper/bin/zkEnv.sh
    sed -i 's/ZOO_LOG4J_PROP=\"INFO,CONSOLE\"/ZOO_LOG4J_PROP=\"INFO,ROLLINGFILE\"/g' /oriental/zookeeper/bin/zkEnv.sh
    sed -i 's/zookeeper.root.logger=INFO, CONSOLE/zookeeper.root.logger=INFO, ROLLINGFILE/g' /oriental/zookeeper/conf/log4j.properties

    echo '清理安装文件，请稍等...'
    sleep 3
    rm zookeeper-3.4.10.tar.gz

    echo '安装zookeeper完成.'
 ;;

 start)
    echo "./zkServer.sh start"
    sh $zkServer start
 ;;

 stop)
    echo "./zkServer.sh stop"
    sh $zkServer stop
 ;;

 restart)
    echo "./zkServer.sh restart"
    sh $zkServer restart
 ;;

 status)
    echo "./zkServer.sh status"
    sh $zkServer status
 ;;

 *)
 echo "Usage: {start|stop|restart|status|install}" >&2
 exit 3
 ;;
esac