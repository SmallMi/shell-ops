#!/usr/bin/env bash
##  2018-03-26
## http://www.smallmi.com
##email: 1374409400@qq.com
##Seven Chou
##centos 6

activemq=/oriental/activemq/bin/activemq

case "$1" in
 install)
    resp_code=`curl -I -m 10 -o /dev/null -s -w %{http_code} "http://192.168.201.53:8888/apache-activemq-5.9.1-bin.tar.gz"`

    echo '开始下载apache-activemq，请稍等...'
    sleep 3
    if [ $resp_code == 200 ];then
        wget http://192.168.201.53:8888/apache-activemq-5.9.1-bin.tar.gz
    else
        wget http://archive.apache.org/dist/activemq/5.9.1/apache-activemq-5.9.1-bin.tar.gz
    fi

    echo '开始解压apache-activemq到/oriental/并重命名，请稍等...'
    sleep 3
    tar fxz apache-activemq-5.9.1-bin.tar.gz -C /oriental/
    mv /oriental/apache-activemq-5.9.1 /oriental/activemq

    echo '开始修改apache-activemq配置，请稍等...'
    sleep 3
    sed -i "s/log4j.appender.logfile.file=\${activemq.base}\/data\/activemq.log/log4j.appender.logfile.file=\/ori-data\/activemq\/logs\/activemq.log/g" /oriental/activemq/conf/log4j.properties
    sed -i "s/log4j.appender.audit.file=\${activemq.base}\/data\/audit.log/log4j.appender.audit.file=\/ori-data\/activemq\/logs\/audit.log/g" /oriental/activemq/conf/log4j.properties
    sed -i "s/ACTIVEMQ_DATA=\"\$ACTIVEMQ_BASE\/data\"/ACTIVEMQ_DATA=\"\/ori-data\/activemq\/data\"/g" /oriental/activemq/bin/activemq

    mkdir -p /ori-data/activemq/data
    mkdir -p /ori-data/activemq/logs

    echo '开始清理临时文件...'
    sleep 3
    rm apache-activemq-5.9.1-bin.tar.gz

    echo '安装ActiveMQ完成.'
 ;;

 start)
    sh $activemq start
 ;;

  stop)
    sh $activemq stop
 ;;

  restart)
    sh $activemq restart
 ;;

  status)
    sh $activemq status
 ;;

  *)
 echo "Usage: {start|stop|restart|status|install}" >&2
 exit 3
 ;;
esac