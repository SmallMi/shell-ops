#!/usr/bin/env bash
##  2018-03-26
## http://www.smallmi.com
##email: 1374409400@qq.com
##Seven Chou
##centos 6

mongodb_path=/oriental/mongodb/bin

case "$1" in
 install)
    resp_code=`curl -I -m 10 -o /dev/null -s -w %{http_code} "http://192.168.201.53:8888/mongodb-linux-x86_64-subscription-rhel62-2.4.14.tgz"`

    echo '开始下载mongodb，请稍等...'
    sleep 3
    if [ $resp_code == 200 ];then
        wget http://192.168.201.53:8888/mongodb-linux-x86_64-subscription-rhel62-2.4.14.tgz
    else
        wget http://downloads.10gen.com/linux/mongodb-linux-x86_64-subscription-rhel62-2.4.14.tgz
    fi

    echo '开始解压并部署mongodb...'
    sleep 3
    tar -zxf mongodb-linux-x86_64-subscription-rhel62-2.4.14.tgz -C /oriental/
    mv /oriental/mongodb-linux-x86_64-subscription-rhel62-2.4.14/ /oriental/mongodb

    echo '创建mongodb数据库目录及日志目录...'
    sleep 3
    mkdir -p /ori-data/mongodb/logs
    mkdir -p /ori-data/mongodb/data

    echo '开始下载mongodb.conf配置文件...'
    sleep 3
    wget http://192.168.201.53:8888/conf/mongodb.conf
    mv mongodb.conf /oriental/mongodb/

    echo '清理安装时的临时文件...'
    sleep 3
    rm mongodb-linux-x86_64-subscription-rhel62-2.4.14.tgz

    echo '安装MongoDB完成.'
 ;;

  start)
    cd $mongodb_path
    ./mongod --config /oriental/mongodb/mongodb.conf
    echo 'MongoDB started'
 ;;

  stop)
    pid=`ps -ef | grep "/oriental/mongodb/mongodb.conf" | grep -v grep | awk '{print $2}'`
    kill $pid
    echo 'MongoDB Stop'
 ;;

  status)
  pid=`ps -ef | grep "/oriental/mongodb/mongodb.conf" | grep -v grep | awk '{print $2}'`
  if [ ! -n "$pid" ];then
    echo 'MongoDB not running'
  else
    echo 'MongoDB is running'
  fi

 ;;

  *)
 echo "Usage: {start|stop|status|install}" >&2
 exit 3
 ;;
esac